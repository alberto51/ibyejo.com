<?php 
require_once("include/DB.php"); ?>
<?php require_once("include/Sessions.php"); ?>
<?php require_once("include/functions.php"); ?>

<?php Confirm_Login(); ?>

<?php 
if (isset($_POST["Submit"])) {
    $Category=mysqli_real_escape_string($conn, $_POST["Category"]);
    date_default_timezone_set("Africa/Kigali");
$CurrentTime=time();
$DateTime=strftime("%d-%B-%Y %H:%M:%S",$CurrentTime);
$DateTime;
$Admin=$_SESSION["Username"];
if(empty($Category)) {
    $_SESSION["ErrorMessage"]="All field must be filled out";
    Redirect_to("categories.php");

}elseif(strlen($Category)>99) {
    $_SESSION["ErrorMessage"]="Too long Name for Category";
    Redirect_to("categories.php");
}else{

    global $conn;
    $Query="INSERT INTO category(datetime,name,creatorname)
    VALUES('$DateTime','$Category','$Admin')";
    $Execute=mysqli_query($conn,$Query);
    if ($Execute) {
         $_SESSION["SuccessMessage"]="Category Added successfully";
    Redirect_to("categories.php");
         
    }else{
         $_SESSION["ErrorMessage"]="Category failed to add";
    Redirect_to("categories.php");
    }

}

}

?>




<html>
    <head>
        <title>Manage Category</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/jquery-3.5.0.min.js"></script>

        <script src="js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="css/adminstyles.css">


                  <style>
                .FieldInfo{
                    color: rgb(251, 174, 44);
                    font-family: Bitter,Georgia,"Times New Roman",Times,serif;
                    font-size: 1.2em;
                    

                </style>




    </head>
    <body>

         <div style="height: 10px; background: #27aae1;"></div>
        <nav class="navbar navbar-inverse" role="navigation">

            <div class="container">

                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>


                    </button>
                 <a class="navbar-brand" href="blog.php">   
                    <img style="margin-top: -15px;" src="images/Capture7.PNG" width=80; height=50>
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="collapse">

                <ul class="nav navbar-nav">
                    <li><a href="#">Home</a></li>
                    <li class="active"><a href="blog.php" target="_blank">Blog</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Features</a></li>
                </ul>


                <form action="blog.php" class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search" name="Search">
                    </div>
                    <button class="btn btn-default" name="SearchButton">Go</button>
                </form>
                </div>



            </div>
            
        </nav>

            <div class="Line" style="height: 10px; background: #27aae1;"></div>


        <div class="container-fluid">
            <div class="row">
                
                <div class="col-sm-2">


                    <ul id="Side-Menu" class="nav nav-pills nav-stacked">
                        <li><a href="Dashboard.php"><span class="glyphicon glyphicon-th"></span>&nbsp;Dashboard</a></li>
                         <li><a href="AddNewPost.php"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Add new Post</a></li>
 <li  class="active"><a href="categories.php"><span class="glyphicon glyphicon-tags"></span>&nbsp;Categories</a></li>
 <li><a href="admins.php"><span class="glyphicon glyphicon-user"></span>&nbsp;Manage Admin</a></li>
 <li><a href="Dashboard.php"><span class="glyphicon glyphicon-comment"></span>&nbsp;Comments</a></li>
 <li><a href="Dashboard.php"><span class="glyphicon glyphicon-equalizer"></span>&nbsp;Live Blog</a></li>
 <li><a href="Logout.php"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Logout</a></li>
`

                    </ul>
                </div>




                <!-- Ending of side area -->

                <div class="col-sm-10"> 
                   <h1>Manage Categories</h1>
                   <?php echo Message(); 
                         echo SuccessMessage();

                        ?>

                    <div>
                        
                        <form action="categories.php" method="post">

                            <fieldset>
                                <div class="form-group">
                                <label for="categoryName"><span class="FieldInfo">Name:</span></label>

                                <input class="form-control" type="text" name="Category" id="categoryname" placeholder="Name">

                                </div>
                                <br>

                                <input class="btn btn-success btn-block" type="submit" name="Submit" value="Add New Category">
                            </fieldset>   
                            </br>                         


                        </form>
                    </div> 


                <div class="table-responsive">

                <table class="table table-striped table-hover">
                    <tr>
                        <th>Sr No.</th>
                        <th>Date & Time</th>
                        <th>Category Name</th>
                        <th>Creator Name</th>
                        <th>Action</th>

                    </tr>

                    <?php 
                    global $conn;
                    $ViewQuery="SELECT * FROM category ORDER BY id desc";
                    $Execute=mysqli_query($conn,$ViewQuery);
                    $SrNo=0;
                    while ($DataRows=mysqli_fetch_array($Execute)) {
                        $Id=$DataRows["id"];
                        $DateTime=$DataRows["datetime"];
                        $Name=$DataRows["name"];
                        $CreatorName=$DataRows["creatorname"];
                        $SrNo++;
                        
                    
                     ?>
                     <tr>
                         <td><?php echo $SrNo; ?></td>
                         <td><?php echo $DateTime; ?></td>
                         <td><?php echo $Name; ?></td>
                         <td><?php echo $CreatorName; ?></td>
                         <td><a href="DeleteCategory.php?id=<?php echo $Id; ?>"><span class="btn btn-danger">Delete</span></a></td>

                     </tr>
                 <?php } ?>
                    

                </table>
                </div>
            </div>

                <!-- Ending of main area -->
            </div>

            <!-- Ending of row-->
        </div>
            <!-- Ending of container-->

<div id="footer">
    <hr>
    <p>Theme by | Gikundiro koloni | &copy;2019-2020 ---- Allright reserved.</p>
    <a style="color: white; text-decoration: none; cursor: pointer; font-weight: bold;" href="#">
        
<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>

    </a>
   
</div>




    </body>
    </html>