<?php 
require_once("include/DB.php"); ?>
<?php require_once("include/Sessions.php"); ?>
<?php require_once("include/functions.php"); ?>

<?php 
if (isset($_POST["Submit"])) {
    $Username=mysqli_real_escape_string($conn, $_POST["Username"]);
    $Password=mysqli_real_escape_string($conn, $_POST["Password"]);


if(empty($Username) || empty($Password)) {
    $_SESSION["ErrorMessage"]="All field must be filled out";
    Redirect_to("Login.php");

}else{

    $Found_Account=Login_Attempt($Username,$Password);
    $_SESSION["User_Id"]=$Found_Account["id"];
    $_SESSION["Username"]=$Found_Account["username"];
    if ($Found_Account) {
         $_SESSION["SuccessMessage"]="Welcome {$_SESSION["Username"]}";
    Redirect_to("dashboard.php");

    }else{
         $_SESSION["ErrorMessage"]="Invalid Username Or Password";
    Redirect_to("Login.php");
    }

   
}

}

?>



<!DOCTYPE html>

<html lang="en">
    <head>
        <title>Manage Category</title>

         <meta charset="UTF-8">
  <meta name="author" content="Niyigena Alberto">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="description" content="tech we news">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript,php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/jquery-3.5.0.min.js"></script>

        <script src="js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="css/adminstyles.css">


                  <style>
                .FieldInfo{
                    color: rgb(251, 174, 44);
                    font-family: Bitter,Georgia,"Times New Roman",Times,serif;
                    font-size: 1.2em;

                    }

                    body{
                        background-color: white;
                    }
                    

                </style>




    </head>
    <body>

         <div style="height: 10px; background: #27aae1;"></div>
        <nav class="navbar navbar-inverse" role="navigation">

            <div class="container">

                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>


                    </button>
                 <a class="navbar-brand" href="blog.php">   
                    <img style="margin-top: -15px;" src="images/Capture7.PNG" width=80; height=50>
                    </a>
                </div>





            </div>
            
        </nav>

            <div class="Line" style="height: 10px; background: #27aae1;"></div>


        <div class="container-fluid">
            <div class="row">
                
               



                <!-- Ending of side area -->

                <div class="col-sm-offset-4 col-sm-4"> 
                    <?php echo Message(); 
                         echo SuccessMessage();

                        ?>
                        <br>
                        <br>
                        <br>
                        <br>


                   <h2>Welcome back !</h2>
                   
                    <div>
                        
                        <form action="Login.php" method="post">

                            <fieldset>
                                <div class="form-group">
                                   
                                <label for="categoryName"><span class="FieldInfo">UserName:</span></label>

                                 <div class="input-group input-group-lg">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-envelope text-primary"></span>
                                        </span>

                                <input class="form-control" type="text" name="Username" id="Username" placeholder="Username">
                                </div>
                                </div>

                                <div class="form-group">
                                <label for="categoryName"><span class="FieldInfo">Password:</span></label>

                                 <div class="input-group input-group-lg">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-lock text-primary"></span>
                                        </span>

                                <input class="form-control" type="Password" name="Password" id="Password" placeholder="Password">
                            </div>

                                </div>
                                <br>

                                <input class="btn btn-info btn-block" type="submit" name="Submit" value="Login">
                            </fieldset>   
                            </br>                         


                        </form>
                    </div> 


            </div>

                <!-- Ending of main area -->
            </div>

            <!-- Ending of row-->
        </div>
            <!-- Ending of container-->




    </body>
    </html>