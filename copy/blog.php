<?php require_once("include/DB.php"); ?>
<?php require_once("include/Sessions.php"); ?>
<?php require_once("include/functions.php"); ?>


<!DOCTYPE>
<html>
    <head>
        <title>Blog Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/is/all.js"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css">
                <link rel="stylesheet" href="css/my-blog-design.css">

        <script src="js/jquery-3.5.0.min.js"></script>

        <script src="js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="css/blogstyle.css">
        
<style>
  #footer-area{
    padding: 90px 0 60px 0;
    background: #000;
    color: #fff;
      font-family: 'Times New Roman', Times, serif;

}
.footer-social{
    margin-top: 20px;

}
.single-footer h3{
    font-weight: 500;
    margin-bottom: 25px;
    color: #fafafa;
  font-family: 'Times New Roman', Times, serif;
}
.link-area li{
  padding: 5px 5px 5px;
  list-style: none;
}
.link-area li a{
  text-transform: capitalize;
  color: #fff;
  font-family: 'Times New Roman', Times, serif;
}

.link-area li a i{
  margin-right: 10px;
  color: #e40c78;
}
.link-area{
  padding: 0;
}
.single-footer p{
  font-family: 'Times New Roman', Times, serif;
}
.footer-social li a{
  width: 30px;
  height: 30px;
  display: inline-block;
  background: #000;

}

.footer-social li a i{
  color: #e40c78;
  padding: 8px;
}
.widget li{
  float: left;
  width: 50%;
}
.copyright-area{
  background: #262626;
  padding: 10px 0;
  margin-top: 30px;
  border-radius: 5px;
  margin-bottom: -59.9px;
  border-right: none;border-left: none;-moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;

}
.copyright-area p{
  font-weight: 600;
  color: #e40c78;
  margin-top: 20px;
}
.copyright-header{
  background: #000;
  padding: 30px 0;
  margin-top: 1px;
  border-radius: 5px;
}
.copyright-header p{
  font-weight: bold;
  color: #e40c78;
  font-size: 80px;
  font-family: 'Times New Roman', Times, serif;
}
.search-box{
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  height: 40px;
  border-radius: 40px;
  padding: 10px;
  width: 18em;
  margin-left: -152px;
}

 .search-box:hover > .search-text{
width: 240px;
padding: 0 6px;
}


.search-btn{
  color: #e40c78;
  float: right;
  width: 40px;
  height: 40px;
  border-radius: 50%;
  background: #2f3640;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 8px;
}

.search-text{
  border:none;
  background: none;
  outline: none;
  float: left;
  padding: 0;
  color: white;
  font-size: 16px;
  transition: 0.4s;
  line-height: 40px;
  margin-top: 8px;
  width: 0px;
  background: #2f3640;
  border-radius: 10px;
  transition: 0.4s;
}


@media(max-width: 375px) {
.img-responsive{
  width: 100px;
  height: 100px !important;
  margin-right: 80px;

  }
  div.thumbnail{
  width: 343px;
  height: 80px !important;
  margin-left: -108px !important;
  background-color: #ffffff;
  border-right: none;
  border-bottom: none;
  margin-top: -15px;
  -moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;

  }


  div.big-post{
     width: 343px !important;
  height: 580px !important;
  margin-left: -108px !important;
  background-color: #ffffff;
  border-right: none;
  border-bottom: none;
  margin-top: -15px;
  -moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;
  }
  #head{
font-size: 10px;
}

@media screen and(max-width: 375px) {
#head{
font-size: 10px;
}
  
</style>

    </head>
   
    <body style="background-color: #000000;">
         <div class="h-img-b-view" width="100%;" hight="80px;">
        <img src="images/yes.jpg" style="height: 278px;object-fit: cover;margin-left: -2px;width: 99.9%;margin-right: 3px;">
    </div>
        <nav class="navbar" role="navigation" style="background-color: #000000; border: none;border: 0.5px solid gray; width: 99.9%;border-right: none;border-left: none;-moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;">

            <div class="container">

                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>


                    </button>
                 <a class="navbar-brand" href="blog.php?Page=1">   
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="collapse">

                <ul class="nav navbar-nav">
                    <li id="nav" class="active"><a href="blog.php?Page=1"><span id="nav" style="color: #ffffff;font-family: 'Times New Roman', Times, serif; font-size: 17px;">HOME</span>
</a></li>
                   <?php
                        global $conn;
                        $ViewQuery="SELECT * FROM category ORDER BY id desc";
                        $Execute=mysqli_query($conn,$ViewQuery);
                        while ($DataRows=mysqli_fetch_array($Execute)) {
                            $Id=$DataRows['id'];
                            $Category=$DataRows['name'];
                         ?>
                         <li class="category"><a href="nav-button-category.php?Category=<?php
                         echo $Category; ?>">
                         <span id="nav" style="color: #ffffff;
  font-family: 'Times New Roman', Times, serif;font-size: 15px;text-transform: uppercase;"><?php echo $Category."<br>"; ?></span>
                     </a>
                 </li>
                     <?php }?>
                     <li><a href=""><i class="fa fa-facebook" style="color: #e40c78"></i></a></li>
               <li><a href=""><i class="fa fa-twitter" style="color: #e40c78"></i></a></li>
                  <li><a href="https://www.youtube.com/channel/UCEpOncAgr__mb4fqOTo70bA?view_as=subscriber"><i class="fa fa-youtube" style="color: #e40c78"></i></a></li>
                     <li style="margin-left: 280px;">
                      <form action="s-content-r.php?Page=1" class="navbar-form navbar-right">

                       <div class="search-box">
                        
                    
                        <input type="text" class="search-text" placeholder="Search" name="Search">
                    
                    <button class="search-btn" name="SearchButton"><i class="fa fa-search"></i></button>
                </form>

                </li>
                         
                       </div>

                     </li>

                </ul>


               
                </div>



            </div>
            
        </nav>

       
      <!-- Modal content-->
     
          
     
 
  
        <br>
        <br>
        <br>

        <svg style="width: 100%" height="733">
  <defs>
    <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
      <stop offset="0%"
      style="stop-color:rgb(255,95,0);stop-opacity:1" />
      <stop offset="100%"
      style="stop-color:rgb(225,0,255);stop-opacity:1" />
    </linearGradient>
  </defs>
  <rect cx="100" width="1349" height="5000" fill="url(#grad1)" />
   <foreignobject class="node"  width="1349" height="7000">

    <div class="main" style="width: 100%;background-color: transparent;height: 854px;margin-left: 5px;margin-top: 2px">
           
            <div class="col-sm-6" style="background-color:transparent;height: 350px;">
               <?php
                        global $conn;
                        $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,1";
                        $Execute=mysqli_query($conn,$ViewQuery);
                        while ($DataRows=mysqli_fetch_array($Execute)) {
                            $Id=$DataRows["id"];
                            $Title=$DataRows["title"];
                            $DateTime=$DataRows["datetime"];
                            $Image=$DataRows["image"];
                            if (strlen($DateTime)>12) {
                                $DateTime=substr($DateTime, 0,12);
                            }
                         ?>

                         <div class="main-inf" style="background-color: #000000;width: 103%;margin-left: -18px;height: 528px">
                             <a class="p-f-m" href="FullPost.php?id=<?php echo $Id; ?>" style="text-decoration: none;object-fit: cover">
                             <img class="f-b-p" style="width: 100%;height: 450px;" src="Upload/<?php echo htmlentities($Image) ?>">

                            
                                <p class="para-first" id="head" style="color: #ffffff;font-family:'Times New Roman', Times, serif;margin-left: 10px">
                                    <?php
                              if(strlen($Title)>40){$Title=substr($Title,0,250).'....';}

                              echo htmlentities($Title); ?>


                                    </p>
                             </a>
                             <br>
                             <br>
                            
                             

                         </div>

                         <?php } ?> 
                
            </div>
            <div class="col-sm-6" style="width:49.9%;background-color: #000000;height: 175px;left: -6px">
                 <?php
                        global $conn;
                        $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 1,1";
                        $Execute=mysqli_query($conn,$ViewQuery);
                        while ($DataRows=mysqli_fetch_array($Execute)) {
                            $Id=$DataRows["id"];
                            $Title=$DataRows["title"];
                            $DateTime=$DataRows["datetime"];
                            $Image=$DataRows["image"];
                            if (strlen($DateTime)>12) {
                                $DateTime=substr($DateTime, 0,12);
                            }
                         ?>

                         <div style="width: 100%;margin-left: -21px;height: 175px">
                            <a href="FullPost.php?id=<?php echo $Id; ?>" style="text-decoration: none;">

                             <img class="pull-left" style="height: 175px;width: 43.7%" src="Upload/<?php echo htmlentities($Image) ?>">

                                <p id="head" style="margin-left: 290px;color: #ffffff;font-family:'Times New Roman', Times, serif;">
                                    <?php

                              echo htmlentities($Title); ?>


                                    </p>
                             </a>
                             <p class="description" style="margin-left: 290px;font-family: 'Times New Roman', Times, serif;color: #e40c78;margin-top: -8px"><?php echo htmlentities($DateTime); ?></p>
                                                     

                         </div>

                         <?php } ?> 
            </div >
             <div class="col-sm-6" style="width:48.1em;background-color: #000000;height: 175px;margin-top: 1px;left: -6px">
                 <?php
                        global $conn;
                        $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 2,1";
                        $Execute=mysqli_query($conn,$ViewQuery);
                        while ($DataRows=mysqli_fetch_array($Execute)) {
                            $Id=$DataRows["id"];
                            $Title=$DataRows["title"];
                            $DateTime=$DataRows["datetime"];
                            $Image=$DataRows["image"];
                            if (strlen($DateTime)>12) {
                                $DateTime=substr($DateTime, 0,12);
                            }
                         ?>

                         <div style="width: 47.8em;margin-left: -21px;height: 170px;margin-top: 0px">
                            <a href="FullPost.php?id=<?php echo $Id; ?>" style="text-decoration: none;">

                             <img class="pull-left" style="height: 175px;width: 280px" src="Upload/<?php echo htmlentities($Image) ?>">

                                <p id="head" style="margin-left: 290px;color: #ffffff;font-family: 'Times New Roman', Times, serif">
                                    <?php
                              

                              echo htmlentities($Title); ?>


                                    </p>
                             </a>
                             <p class="description" style="margin-left: 290px;color: #e40c78;font-family: 'Times New Roman', Times, serif"><?php echo htmlentities($DateTime); ?></p>
                                                      

                         </div>

                         <?php } ?> 
            </div>

             <div class="col-sm-6" style="width:48.5em;background-color: #000000;height: 176px;left: 663px;margin-top: 1px">
                 <?php
                        global $conn;
                        $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 3,1";
                        $Execute=mysqli_query($conn,$ViewQuery);
                        while ($DataRows=mysqli_fetch_array($Execute)) {
                            $Id=$DataRows["id"];
                            $Title=$DataRows["title"];
                            $DateTime=$DataRows["datetime"];
                            $Image=$DataRows["image"];
                            if (strlen($DateTime)>12) {
                                $DateTime=substr($DateTime, 0,12);
                            }
                         ?>

                         <div style="width: 47.8em;margin-left: -15px;height: 175px">
                            <a href="FullPost.php?id=<?php echo $Id; ?>" style="text-decoration: none;">

                             <img class="pull-left" style="height: 176px;width: 280px" src="Upload/<?php echo htmlentities($Image) ?>">

                                <p id="head" style="margin-left: 290px;color: #ffffff;font-family:'Times New Roman', Times, serif;">
                                    <?php

                              echo htmlentities($Title); ?>


                                    </p>
                             </a>
                             <p class="description" style="margin-left: 290px;font-family: 'Times New Roman', Times, serif;color: #e40c78;margin-top: -8px"><?php echo htmlentities($DateTime); ?></p>
                                                     

                         </div>

                         <?php } ?> 
            </div >     
        </div>
                
            </foreignobject>
</svg>



            <div class="container" style="background-color: white;width: 97% "><!---Container--->

            <div class="row" style="margin-left: 80px;"><!--- Row--->



                <div class="col-sm-8 main-blog" style="margin-top: 20px;width: 70%"><!---Main Blog Area--->
               

               <?php

               global $conn;

               if (isset($_GET["SearchButton"])) {
                   $Search=$_GET["Search"];
                   $ViewQuery="SELECT * FROM admin_panel WHERE id LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";

               }
//query when category is active
               elseif (isset($_GET["Category"])) {
                $Category=$_GET["Category"];
                $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc ";  
               }
               elseif (isset($_GET["Page"])) {
                $Page=$_GET["Page"];
                if ($Page==0 ||$Page<1) {
                    $ShowPostFrom=0;   
                }else{


                $ShowPostFrom=($Page*5)-5;}
               

                $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 4,5";
               


               }else{
               $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,3";}

               $Execute=mysqli_query($conn,$ViewQuery);
               while ($DataRows=mysqli_fetch_array($Execute)) {
                   $PostId=$DataRows["id"];
                   $DataTime=$DataRows["datetime"];
                   $title=$DataRows["title"];
                   $Category=$DataRows["category"];
                   $Admin=$DataRows["author"];
                   $Image=$DataRows["image"];
                   $Post=$DataRows["post"];
               
                ?>

                <div class="blogpost thumbnail" width=200; height=100;  >
                    <a href="FullPost.php?id=<?php echo $PostId; ?>" style="text-decoration: none;color: black;font-family:'Times New Roman', Times, serif;">
                    <img class="img-responsive pull-left" width=200; height=900; style="position: relative;margin-top: -20px;left: -10px;" src="Upload/<?php echo  $Image; ?>">
</a>
                    <div class="caption">
                        <a href="FullPost.php?id=<?php echo $PostId; ?>" style="text-decoration: none;color: black;font-family:'Times New Roman', Times, serif;">

                        <h1 id="head" style="margin-left: 10px;margin-top: -8px;margin-left: 10px;font-size: 20px;font-family:'Times New Roman', Times, serif;"><?php echo htmlentities($title); ?></h1>
                    </a>

                      

                       
                        <a href="FullPost.php?id=<?php echo $PostId; ?>" style="text-decoration: none;color: black;font-family:'Times New Roman', Times, serif;color: #e40c78">

                          <p class="description" style="font-weight: bold;font-family:'Times New Roman', Times, serif;"> Published on <?php echo htmlentities($DataTime)  ?>
                            
</a>
                             <?php 
                                 $conn;
                                 $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
                                 $ExecuteApproved=mysqli_query($conn,$QueryApproved);
                                 $RowsApproved=mysqli_fetch_array($ExecuteApproved);
                                 $TotalApproved=array_shift($RowsApproved);
                                 if ( $TotalApproved>0) {
                                    
                                 
                                  ?>

 </p>
                                 <span class=" " style=" font-family:'Times New Roman', Times, serif;">
                                    <?php echo $TotalApproved; ?> Comments 
                                        
                                    </span>

                                <?php } ?>
                       
                    </div>
                   <!-- <a href="FullPost.php?id=<?php echo $PostId; ?>"><span class="btn btn-info"> Read More &rsaquo;&rsaquo;</span></a>
                   -->


                </div>


            <?php } ?>

               

               <?php

               global $conn;

               if (isset($_GET["SearchButton"])) {
                   $Search=$_GET["Search"];
                   $ViewQuery="SELECT * FROM admin_panel WHERE id LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";

               }
//query when category is active
               elseif (isset($_GET["Category"])) {
                $Category=$_GET["Category"];
                $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc ";  
               }
               elseif (isset($_GET["Page"])) {
                $Page=$_GET["Page"];
                if ($Page==0 ||$Page<1) {
                    $ShowPostFrom=0;   
                }else{


                $ShowPostFrom=($Page*5)-5;}
               

                $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 9,1";
               


               }else{
               $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,3";}

               $Execute=mysqli_query($conn,$ViewQuery);
               while ($DataRows=mysqli_fetch_array($Execute)) {
                   $PostId=$DataRows["id"];
                   $DataTime=$DataRows["datetime"];
                   $title=$DataRows["title"];
                   $Category=$DataRows["category"];
                   $Admin=$DataRows["author"];
                   $Image=$DataRows["image"];
                   $Post=$DataRows["post"];
               
                ?>
 <a href="FullPost.php?id=<?php echo $PostId; ?>" style="text-decoration: none;color: black;">

                <div class="blogpost big-post" style="background-color: #000000;width: 88.8%;height: 39.5em;">
                   <div class="caption" >
                        <a href="FullPost.php?id=<?php echo $PostId; ?>" style="text-decoration: none;color: black;">

                        <h1 id="head" style="text-transform: uppercase;color:#ffffff;margin-left: 10px;margin-top: -8px;margin-left: -8px;font-size: 40px;font-family:'Times New Roman', Times, serif; "><?php echo htmlentities($title); ?></h1>
                    

                      

                       
                        

                          <p class="description" style="margin-left: -8px;font-weight: bold;color: #e40c78;font-family:'Times New Roman', Times, serif;"> Published on <?php echo htmlentities($DataTime)  ?>
                            

                             <?php 
                                 $conn;
                                 $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
                                 $ExecuteApproved=mysqli_query($conn,$QueryApproved);
                                 $RowsApproved=mysqli_fetch_array($ExecuteApproved);
                                 $TotalApproved=array_shift($RowsApproved);
                                 if ( $TotalApproved>0) {
                                    
                                 
                                  ?>
                                   |<span class=" " style="color: gray;font-family:'Times New Roman', Times, serif;">
                                    <?php echo $TotalApproved; ?> Comments 
                                        
                                    </span>

 </p>
                                

                                <?php } ?>
                       
                    </div>
                    <br>
                    <br>
                    
                    <img class="img-responsive pull-left" style="height: 384.5px;position: absolute;margin-top: -20px;left: 16px;width: 85.4%;" src="Upload/<?php echo  $Image; ?>">

                   
                   <!-- <a href="FullPost.php?id=<?php echo $PostId; ?>"><span class="btn btn-info"> Read More &rsaquo;&rsaquo;</span></a>
                   -->


                </div>
</a>
            <?php } ?>
<div style="margin-top: 25px">

              <?php

               global $conn;

               if (isset($_GET["SearchButton"])) {
                   $Search=$_GET["Search"];
                   $ViewQuery="SELECT * FROM admin_panel WHERE id LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";

               }
//query when category is active
               elseif (isset($_GET["Category"])) {
                $Category=$_GET["Category"];
                $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc ";  
               }
               elseif (isset($_GET["Page"])) {
                $Page=$_GET["Page"];
                if ($Page==0 ||$Page<1) {
                    $ShowPostFrom=0;   
                }else{


                $ShowPostFrom=($Page*5)-5;}
               

                $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 10,8";
               


               }else{
               $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,3";}

               $Execute=mysqli_query($conn,$ViewQuery);
               while ($DataRows=mysqli_fetch_array($Execute)) {
                   $PostId=$DataRows["id"];
                   $DataTime=$DataRows["datetime"];
                   $title=$DataRows["title"];
                   $Category=$DataRows["category"];
                   $Admin=$DataRows["author"];
                   $Image=$DataRows["image"];
                   $Post=$DataRows["post"];
               
                ?>

                <div class="blogpost thumbnail" style="background-color: #ffffff;width: 80%;height: 10em;border-right: none;border-bottom: none;margin-top: -15px;-moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;" >
                    <a href="FullPost.php?id=<?php echo $PostId; ?>" style="text-decoration: none;color: black;font-family:'Times New Roman', Times, serif;">
                    <img class="img-responsive pull-left" width=200;height=800; style="height: 120%;position: relative;margin-top: -20px;left: -10px;" src="Upload/<?php echo  $Image; ?>">
</a>
                    <div class="caption" >
                        <a href="FullPost.php?id=<?php echo $PostId; ?>" style="text-decoration: none;color: black;font-family:'Times New Roman', Times, serif;">

                        <h1 id="head" style="margin-left: 10px;margin-top: -8px;margin-left: 10px;font-size: 20px;font-family:'Times New Roman', Times, serif;"><?php echo htmlentities($title); ?></h1>
                    </a>

                      

                       
                        
<a href="FullPost.php?id=<?php echo $PostId; ?>" style="text-decoration: none;color: #e40c78;font-family:'Times New Roman', Times, serif;">
                          <p class="description" style="font-weight: bold;color: #e40c78;font-family:'Times New Roman', Times, serif;"> Published on <?php echo htmlentities($DataTime)  ?>
                            
</a>
                             <?php 
                                 $conn;
                                 $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
                                 $ExecuteApproved=mysqli_query($conn,$QueryApproved);
                                 $RowsApproved=mysqli_fetch_array($ExecuteApproved);
                                 $TotalApproved=array_shift($RowsApproved);
                                 if ( $TotalApproved>0) {
                                    
                                 
                                  ?>

 </p>
                                 <span class=" " style=" font-family:'Times New Roman', Times, serif;">
                                    <?php echo $TotalApproved; ?> Comments 
                                        
                                    </span>

                                <?php } ?>
                       
                    </div>
                   <!-- <a href="FullPost.php?id=<?php echo $PostId; ?>"><span class="btn btn-info"> Read More &rsaquo;&rsaquo;</span></a>
                   -->


                </div>
                


            <?php } ?>


            <?php

               global $conn;

               if (isset($_GET["SearchButton"])) {
                   $Search=$_GET["Search"];
                   $ViewQuery="SELECT * FROM admin_panel WHERE id LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";

               }
//query when category is active
               elseif (isset($_GET["Category"])) {
                $Category=$_GET["Category"];
                $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc ";  
               }
               elseif (isset($_GET["Page"])) {
                $Page=$_GET["Page"];
                if ($Page==0 ||$Page<1) {
                    $ShowPostFrom=0;   
                }else{


                $ShowPostFrom=($Page*5)-5;}
               

                $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 18,1";
               


               }else{
               $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,3";}

               $Execute=mysqli_query($conn,$ViewQuery);
               while ($DataRows=mysqli_fetch_array($Execute)) {
                   $PostId=$DataRows["id"];
                   $DataTime=$DataRows["datetime"];
                   $title=$DataRows["title"];
                   $Category=$DataRows["category"];
                   $Admin=$DataRows["author"];
                   $Image=$DataRows["image"];
                   $Post=$DataRows["post"];
               
                ?>
<a href="FullPost.php?id=<?php echo $PostId; ?>" style="text-decoration: none;color: black;">
                <div class="blogpost thumbnail" style="background-color: #000000;width: 88.8%;height: 39.5em;-moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;border-right: " >
                   <div class="caption" >
                        

                        <h1 id="head" style="text-transform: uppercase;color:#ffffff;margin-left: 10px;margin-top: -8px;margin-left: -8px;font-size: 40px;font-family:'Times New Roman', Times, serif; "><?php echo htmlentities($title); ?></h1>
                   

                      

                       
                        

                          <p class="description" style="margin-left: -8px;font-weight: bold;color: #e40c78;font-family:'Times New Roman', Times, serif;"> Published on <?php echo htmlentities($DataTime)  ?>
                            

                             <?php 
                                 $conn;
                                 $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
                                 $ExecuteApproved=mysqli_query($conn,$QueryApproved);
                                 $RowsApproved=mysqli_fetch_array($ExecuteApproved);
                                 $TotalApproved=array_shift($RowsApproved);
                                 if ( $TotalApproved>0) {
                                    
                                 
                                  ?>
                                   |<span class=" " style="color: gray;font-family:'Times New Roman', Times, serif;">
                                    <?php echo $TotalApproved; ?> Comments 
                                        
                                    </span>

 </p>
                                

                                <?php } ?>



                       
                    </div>
                    <br>
                    <br>
                    
                    <img class="img-responsive pull-left" style="height: 10.8%;position: absolute;margin-top: -20px;left: 16px;width: 85.4%;" src="Upload/<?php echo  $Image; ?>">

                   
                   <!-- <a href="FullPost.php?id=<?php echo $PostId; ?>"><span class="btn btn-info"> Read More &rsaquo;&rsaquo;</span></a>
                   -->


                </div>
                 </a>

            <?php } ?>
<div style="margin-top: 25px">

              <?php

               global $conn;

               if (isset($_GET["SearchButton"])) {
                   $Search=$_GET["Search"];
                   $ViewQuery="SELECT * FROM admin_panel WHERE id LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";

               }
//query when category is active
               elseif (isset($_GET["Category"])) {
                $Category=$_GET["Category"];
                $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc ";  
               }
               elseif (isset($_GET["Page"])) {
                $Page=$_GET["Page"];
                if ($Page==0 ||$Page<1) {
                    $ShowPostFrom=0;   
                }else{


                $ShowPostFrom=($Page*5)-5;}
               

                $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 26,8";
               


               }else{
               $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,3";}

               $Execute=mysqli_query($conn,$ViewQuery);
               while ($DataRows=mysqli_fetch_array($Execute)) {
                   $PostId=$DataRows["id"];
                   $DataTime=$DataRows["datetime"];
                   $title=$DataRows["title"];
                   $Category=$DataRows["category"];
                   $Admin=$DataRows["author"];
                   $Image=$DataRows["image"];
                   $Post=$DataRows["post"];
               
                ?>
                <div class="blogpost thumbnail" style="background-color: #ffffff;width: 72%;height: 10em;border-right: none;border-bottom: none;margin-top: -15px;-moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;" >
                    <a href="FullPost.php?id=<?php echo $PostId; ?>" style="text-decoration: none;color: black;font-family:'Times New Roman', Times, serif;">

                    <img class="img-responsive pull-left" style="width: 30%;height: 120%;position: relative;margin-top: -20px;left: -10px;" src="Upload/<?php echo  $Image; ?>">
</a>
                    <a href="FullPost.php?id=<?php echo $PostId; ?>" style="text-decoration: none;color: black;font-family:'Times New Roman', Times, serif;">

                    <div class="caption" >
                        

                        <h1 id="head" style="margin-left: 10px;margin-top: -8px;margin-left: 10px;font-size: 20px;font-family:'Times New Roman', Times, serif;"><?php echo htmlentities($title); ?></h1>
                   

                      

                       
                        

                          <p class="description" style="font-weight: bold;color: #e40c78;font-family:'Times New Roman', Times, serif;"> Published on <?php echo htmlentities($DataTime)  ?>
                            

                             <?php 
                                 $conn;
                                 $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
                                 $ExecuteApproved=mysqli_query($conn,$QueryApproved);
                                 $RowsApproved=mysqli_fetch_array($ExecuteApproved);
                                 $TotalApproved=array_shift($RowsApproved);
                                 if ( $TotalApproved>0) {
                                    
                                 
                                  ?>

 </p>
                                 <span class=" " style=" font-family:'Times New Roman', Times, serif;">
                                    <?php echo $TotalApproved; ?> Comments 
                                        
                                    </span>

                                <?php } ?>
                       
                    </div>
                     </a>
                   <!-- <a href="FullPost.php?id=<?php echo $PostId; ?>"><span class="btn btn-info"> Read More &rsaquo;&rsaquo;</span></a>
                   -->


                </div>
                


            <?php } ?>


               


           

</div>

</div>

            </div><!---Main Blog Ending--->

                <!---Side bar Area --->

            <div class="col-sm-offset-1 col-sm-3">     

                  <h3 class="c-rock-list__title">
                    Recent Post
                  </h3>
                  <div class="c-rock-list__body">
    
    
      
        
        
         
          
          <span class="c-rock-list__item--body">
            
             <?php
                        global $conn;
                        $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,1";
                        $Execute=mysqli_query($conn,$ViewQuery);
                        while ($DataRows=mysqli_fetch_array($Execute)) {
                            $Id=$DataRows["id"];
                            $Title=$DataRows["title"];
                            $DateTime=$DataRows["datetime"];
                            $Image=$DataRows["image"];
                            if (strlen($DateTime)>12) {
                                $DateTime=substr($DateTime, 0,12);
                            }
                         ?>

                         <div>
                          <a href="FullPost.php?id=<?php echo $Id; ?>" style="text-decoration: none;">
                             <img class="pull-left" style="margin-top: 14px; margin-left: 14px;width: 240px" src="Upload/<?php echo htmlentities($Image) ?>" width=100;height=90;>

                             <a href="FullPost.php?id=<?php echo $Id; ?>" style="text-decoration: none;">
                                <p id="head" style="margin-left: 15px;color: black;font-size: 16px;width: 240px">
                                    <?php
                              if(strlen($Title)>40){$Title=substr($Title,0,40).'....';}

                              echo htmlentities($Title); ?>


                                    </p>
                             
                             <p class="description" style="margin-left: 15px;margin-top: -12px;margin-bottom: -15px"><?php echo htmlentities($DateTime); ?></p>
                                                     
</a>
                         </div>

                         <?php } ?> 
   
            
          </span>
  

          <span class="c-rock-list__item--body">

             <?php
                        global $conn;
                        $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 1,1";
                        $Execute=mysqli_query($conn,$ViewQuery);
                        while ($DataRows=mysqli_fetch_array($Execute)) {
                            $Id=$DataRows["id"];
                            $Title=$DataRows["title"];
                            $DateTime=$DataRows["datetime"];
                            $Image=$DataRows["image"];
                            if (strlen($DateTime)>12) {
                                $DateTime=substr($DateTime, 0,12);
                            }
                         ?>

                         <div>
                          <a href="FullPost.php?id=<?php echo $Id; ?>" style="text-decoration: none;">
                             <img class="pull-left" style="margin-top: 25px; margin-left: 14px;width: 240px" src="Upload/<?php echo htmlentities($Image) ?>" width=100;height=90;>

                             
                                <p id="head" style="margin-left: 15px;color: black;;font-size: 16px;width: 240px">
                                    <?php
                              if(strlen($Title)>40){$Title=substr($Title,0,40).'....';}

                              echo htmlentities($Title); ?>


                                    </p>
                             
                             <p class="description" style="margin-left: 15px;margin-top: -13px;margin-bottom: -16px"><?php echo htmlentities($DateTime); ?></p>
                                                       
</a>
                         </div>

                         <?php } ?> 
   
            
            
                      
          </span>
      
    
    

          <span class="c-rock-list__item--body">
            
             <?php
                        global $conn;
                        $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 2,1";
                        $Execute=mysqli_query($conn,$ViewQuery);
                        while ($DataRows=mysqli_fetch_array($Execute)) {
                            $Id=$DataRows["id"];
                            $Title=$DataRows["title"];
                            $DateTime=$DataRows["datetime"];
                            $Image=$DataRows["image"];
                            if (strlen($DateTime)>12) {
                                $DateTime=substr($DateTime, 0,12);
                            }
                         ?>

                         <div>
                          <a href="FullPost.php?id=<?php echo $Id; ?>" style="text-decoration: none;">

                             <img class="pull-left" style="margin-top: 25px; margin-left: 14px;width: 240px" src="Upload/<?php echo htmlentities($Image) ?>" width=100;height=90;>

                             <a href="FullPost.php?id=<?php echo $Id; ?>" style="text-decoration: none;">
                                <p id="head" style="margin-left: 15px;color: black;;font-size: 16px;width: 240px">
                                    <?php
                              if(strlen($Title)>40){$Title=substr($Title,0,40).'....';}

                              echo htmlentities($Title); ?>


                                    </p>
                             
                             <p class="description" style="margin-left: 15px;margin-top: -14px;"><?php echo htmlentities($DateTime); ?></p>
                             </a>

                         </div>

                         <?php } ?> 
   
            
            
          </span>
   
  <div class="c-rock-list__cta">
    <a href="morePost.php?Page=1" class="c-rock-list__more" style="color: #e40c78;text-decoration: none;font-size: 14px;width: 240px;">
      watch more
    </a>
  </div>


<!--
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h2 class="panel-title">Categories</h2>
                    </div>
                    <div class="panel-body">
                        <?php
                        global $conn;
                        $ViewQuery="SELECT * FROM category ORDER BY id desc";
                        $Execute=mysqli_query($conn,$ViewQuery);
                        while ($DataRows=mysqli_fetch_array($Execute)) {
                            $Id=$DataRows['id'];
                            $Category=$DataRows['name'];
                         ?>
                         <a href="blog.php?Category=<?php
                         echo $Category; ?>">
                         <span id="heading"><?php echo $Category."<br>"; ?></span>
                     </a>
                     <?php }?>
                    </div>
                    <div class="panel-footer"></div>
                </div>

            -->


<!--

                 <div class="panel panel-danger" style="width: 22em;font-family:'Times New Roman', Times, serif;">
                    <div class="panel-heading" style="height: -20px;">
                    </div>
                                            <h2 class="panel-title" style="margin-left: 11px;margin-top: 10px;color: #E40C78;font-weight: bold;">Recent Post</h2>
                    <div class="panel-body">
                        <?php
                        global $conn;
                        $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,5";
                        $Execute=mysqli_query($conn,$ViewQuery);
                        while ($DataRows=mysqli_fetch_array($Execute)) {
                            $Id=$DataRows["id"];
                            $Title=$DataRows["title"];
                            $DateTime=$DataRows["datetime"];
                            $Image=$DataRows["image"];
                            if (strlen($DateTime)>12) {
                                $DateTime=substr($DateTime, 0,12);
                            }
                         ?>

                         <div>
                             <img class="pull-left" style="margin-top: -10px; margin-left: -11px;" src="Upload/<?php echo htmlentities($Image) ?>" width=100;height=90;>

                             <a href="FullPost.php?id=<?php echo $Id; ?>" style="text-decoration: none;">
                                <p id="head" style="margin-left: 95px;color: black;">
                                    <?php
                              if(strlen($Title)>40){$Title=substr($Title,0,40).'....';}

                              echo htmlentities($Title); ?>


                                    </p>
                             </a>
                             <p class="description" style="margin-left: 95px;"><?php echo htmlentities($DateTime); ?></p>
                                                       <hr>

                         </div>

                         <?php } ?> 

                        
                    </div>
                </div>

            </div><!---Side bar Area Ending--->


         </div>  <!---Row Ending--->


</div>   <!---Container Ending--->

</a>
</div>
</span>
</a>
</div>



<footer>
  <div id="footer-area">
    <div class="container">
      <div class="row">

         <div class="container" style="margin-top: -90px">
          <div class="row">
            <div class="col-md-12">
              <div class="copyright-header text-center">
                <p>TheGape</p>
              </div>
            </div>
          </div>
        </div>
        <hr>
        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h3>About thegape</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
            <ul class="footer-social list-inline">
              <li><a href=""><i class="fa fa-facebook"></i></a></li>
               <li><a href=""><i class="fa fa-twitter"></i></a></li>
                <li><a href=""><i class="fa fa-pinterest"></i></a></li>
                 <li><a href=""><i class="fa fa-linkedin "></i></a></li>
                  <li><a href=""><i class="fa fa-youtube"></i></a></li>
            </ul>
            
          </div>
        </div>

        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h3>Support</h3>
            <ul class="link-area">
              <li><a href=""><i class="fa fa-long-arrow-right"></i>Product FAQS</a></li>
               <li><a href=""><i class="fa fa-long-arrow-right"></i>Policies</a></li>
               <li><a href=""><i class="fa fa-long-arrow-right"></i>Carieers</a></li>
               <li><a href=""><i class="fa fa-long-arrow-right"></i>Sitemaps</a></li>
               <li><a href=""><i class="fa fa-long-arrow-right"></i>Contact</a></li>


            </ul>
          </div>
        </div>


        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h3>Our Contact</h3>
            <ul class="link-area">
              <li><a href=""><i class="fa fa-phone"></i>+250786674794</a></li>
               <li><a href=""><i class="fa fa-envelope-o"></i>albertomelviss@gmail.com</a></li>
               <li><a href=""><i class="fa fa-map"></i>Kigali, Rwanda</a></li>
               <li><a href=""><i class="fa fa-globe"></i>thegape.com</a></li>


            </ul>
          </div>
        </div>

         <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h3>Our Contact</h3>
            <ul class="link-area">
              <li><a href=""><i class="fa fa-phone"></i>+250786674794</a></li>
               <li><a href=""><i class="fa fa-envelope-o"></i>albertomelviss@gmail.com</a></li>
               <li><a href=""><i class="fa fa-map"></i>Kigali, Rwanda</a></li>
               <li><a href=""><i class="fa fa-globe"></i>thegape.com</a></li>


            </ul>
          </div>
        </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="copyright-area text-center">
                <p>&copy; 2020, All Right</p>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
    
  </div>


</footer>
    </body>
    </html>

    


