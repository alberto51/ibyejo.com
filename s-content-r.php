<?php require_once("include/DB.php"); ?>
<?php require_once("include/Sessions.php"); ?>
<?php require_once("include/functions.php"); ?>


<!DOCTYPE html>
<html lang="en">


<head>
  
  <title>WebRoomSearch</title>

  <meta charset="UTF-8">
  <meta name="author" content="Niyigena Alberto">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="tech we news">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript,php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/my-blog-design.css">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/main__search_result.css">






  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

  

  <style>
    

    #footer-area{
      padding: 90px 0 60px 0;
      background: #000;
      color: #fff;
      font-family: 'Times New Roman', Times, serif;

    }
    .footer-social{
      margin-top: 20px;

    }
    .single-footer h3{
      font-weight: 500;
      margin-bottom: 25px;
      color: #fafafa;
      font-family: 'Times New Roman', Times, serif;
    }

    .link-area li{
      padding: 5px 5px 5px;
      list-style: none;
    }

    .link-area li a{
      text-transform: capitalize;
      color: #fff;
      font-family: 'Times New Roman', Times, serif;
    }

    .link-area li a i{
      margin-right: 10px;
      color: #e40c78;
    }
    .link-area{
      padding: 0;
    }
    .single-footer p{
      font-family: 'Times New Roman', Times, serif;
    }

    .footer-social li a{
      width: 30px;
      height: 30px;
      display: inline-block;
      background: #000;

    }

    .footer-social li a i{
      color: #e40c78;
      padding: 8px;
    }

    .widget li{
      float: left;
      width: 50%;
    }

    .copyright-area{
      background: #262626;
      padding: 10px 0;
      margin-top: 30px;
      border-radius: 5px;
      margin-bottom: -59.9px;
      border-right: none;border-left: none;-moz-border-radius: 0px;
      -webkit-border-radius: 0px;
      border-radius: 0px;

    }
    .copyright-area p{
      font-weight: 600;
      color: #e40c78;
      margin-top: 20px;
    }
    .copyright-header{
      background: #000;
      padding: 30px 0;
      margin-top: 1px;
      border-radius: 5px;
    }
    .copyright-header p{
      font-weight: bold;
      color: #e40c78;
      font-size: 80px;
      font-family: 'Times New Roman', Times, serif;
    }
    .search-box{
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%,-50%);
      height: 40px;
      border-radius: 40px;
      padding: 10px;
      width: 18em;
      margin-left: -152px;
    }

    .search-box:hover > .search-text{
      width: 240px;
      padding: 0 6px;
    }

    .search-btn{
      color: #e40c78;
      float: right;
      width: 40px;
      height: 40px;
      border-radius: 50%;
      background: #2f3640;
      display: flex;
      justify-content: center;
      align-items: center;
      margin-top: 8px;
    }

    .search-text{
      border:none;
      background: none;
      outline: none;
      float: left;
      padding: 0;
      color: white;
      font-size: 16px;
      transition: 0.4s;
      line-height: 40px;
      margin-top: 8px;
      width: 0px;
      background: #2f3640;
      border-radius: 10px;
    }

    @media only screen and (max-width: 360px){   
    .blog_main-post .blog_item-post{
      width: 100%;
      float: left;
    }
    .blog_main-post{
      width: 100%;
    }
    .aside-column{
      display: none;
    }

    .main-column{
      width: 100%;
    }
  form{
    display: none;
  }

}


  @media only screen and (max-width: 360px) and (min-width: 359px) {

    form{
      display: none;
    }

    .container .mainpost, .container article, .container .card{

    }
    .container .card{
      width: 100%;
    }

    .container article{
      width: 100%;
    }
    .container article img{
      width: 100%;
    }
    .main-blog-area .card{
      width: 100%;
      min-height: 280px;
    }
    .main-blog-area .card img{
      width: 100%;
      float: left;
      max-height: 390px;
    }
    .main-blog-area .card p{
      margin-top: 50px;
    }
    .main-blog-area .card h2{
      margin-top: 120px;
    }
    .main-blog-area .card{
      width: 100%;
      max-height: 390px;
    }
    .main-blog-area .big-card{
      width: 100%;
      max-height: 20px;

    }
    .main-blog-area .big-card .card-body img{
      width: 100%;
      max-height: 900px;

    }

    .main-blog-area .big-card .card-header{
      margin: 0px;
      padding: 0px;
      float: left;
    }
    .main-blog-area .big-card .card-body img{
      width: 40%;
      max-height: 900px;
    }
    .aside-column{
      display: none;
    }

    .main-column{
      width: 100%
    }  
     .main-blog-area .main-post .main-post-data img{
      float: left;
      width: 50%;
    }
    .main-blog-area .main-post .main-post-data h1{
float: right;
  }
}


  @media only screen and (max-width: 375px) {


 form{
    display: none;
  }
    .container .card{
      width: 100%;
    }

    .container article{
      width: 100%;
    }
    .container article img{
      width: 100%;
    }
    .main-blog-area .card{
      width: 100%;
      max-height: 280px;
    }
    .main-blog-area .card img{
      width: 100%;
      float: left;
    }

    .main-blog-area .big-card{
      width: 100%;
      max-height: 900px;

    }
    .main-blog-area .big-card .card-header h2,.main-blog-area .big-card .card-header p{
      margin: 0px;
      padding: 0px;
    }
    .main-blog-area .big-card .card-body img{
      width: 100%;

    }

    .main-blog-area .big-card .card-header{
      margin: 0px;
      padding: 0px;
      float: left;
    }
    .main-blog-area .big-card .card-body img{
      width: 40%;
    }

    .blog_main-post .blog_item-post{
      width: 100%;
      float: left;
    }

    .blog_main-post{
      width: 100%;
    }
    .aside-column{
      display: none;
    }

    .main-column{
      width: 100%
    }
     .main-blog-area .main-post .main-post-data img{
      float: left;
      width: 50%;
    }
    .main-blog-area .main-post .main-post-data h1{
      float: right;
    }

  }

  @media only screen and (max-width: 411px) {

     form{
    display: none;
  }
    .container .card{
      width: 100%;
    }

    .container article{
      width: 100%;
    }
    .container article img{
      width: 100%;
    }
    .main-blog-area .card{
      width: 100%;
      max-height: 280px;
    }
    .main-blog-area .card img{
      width: 100%;
      float: left;
    }
    .main-blog-area .card p{
      margin-top: 50px;
    }
    .main-blog-area .card h2{
      margin-top: 120px;
    }
    .main-blog-area .card{
      width: 100%;
      max-height: 280px;
    }
    .main-blog-area .big-card{
      width: 100%;
      max-height: 900px;

    }

    .main-blog-area .big-card .card-header h2,.main-blog-area .big-card .card-header p{
      margin: 0px;
      padding: 0px;
    }
    .main-blog-area .big-card .card-body img{
      width: 100%;

    }

    .blog_main-post .blog_item-post{
      width: 100%;
      float: left;
    }

    .blog_main-post{
      width: 100%;
    }

    div .btn_post{
      margin-top: 20px;
      margin-bottom: 8px;
      margin-left: 0px;
      width: 100%;
      float: right;
    }
    .look_more-post{
      text-decoration: none;
      color: #ffffff;
      background-color: #e40c78;
      border-radius: 20px;
      padding-left: 50px;
      padding-right: 50px;
      padding-top: 15px;
      padding-bottom: 15px;
      font-size: 15px;
      margin-bottom: 55px;
    }
    .aside-column{
      display: none;
    }

    .main-column{
      width: 100%
    }
     .main-blog-area .main-post .main-post-data img{
      float: left;
      width: 50%;
    }
     form{
    display: none;
  }

    
  }


  @media only screen and (max-width: 520px) {
form{
  display: none;
}

}
  @media only screen and (max-width: 760px) {






form{
  margin-left: 80px;
}

section .container{
  width: 100%;
}

    .container .card{
      width: 100%;
    }
    .container article{
      width: 100%;
    }
    .container article img{
      width: 100%;
    }
    .main-blog-area .card{
      width: 100%;
      max-height: 280px;
    }
    .main-blog-area .big-card{
      width: 80%;
      max-height: 900px;

    }
    .main-blog-area .big-card .card-header h2,.main-blog-area .big-card .card-header p{
      margin: 0px;
      padding: 0px;
    }
    .main-blog-area .big-card .card-body img{
      width: 100%;
      
    }

    .blog_main-post .blog_item-post{
      width: 100%;
      float: left;
    }

    .blog_main-post{
      width: 100%;
    }
    .aside-column{
      display: none;
    }
    .main-column{
      width: 100%
    }
     .main-blog-area .main-post .main-post-data img{
      float: left;
      width: 50%;
    }

  }


  @media only screen and (min-width: 415px) and (max-width: 1040px){
    .main-blog-area .card{
      width: 70%;
      max-height: 180px;
    }
    .main-blog-area .card img{
      width: 40%;
      float: left;
    }
    .main-blog-area .big-card{
      width: 80%;
      max-height: 900px;
    }
    .main-blog-area .card{
     border-top: 0.5px solid #2b2d32;
   }
   .main-blog-area .big-card .card-header{
    margin: 0px;
    padding: 0px;
  }
  .main-blog-area .big-card .card-body img{
    width: 40%;
  }
  .main-blog-area .big-card .card-header h2,.main-blog-area .big-card .card-header p{
    margin: 0px;
    padding: 0px;
  }
  .main-blog-area .big-card .card-body img{
    width: 100%; 
  }
  .blog_main-post .blog_item-post{
    width: 100%;
    float: left;
  }
  .blog_main-post{
    width: 80%;
  }
   .aside-column{
      display: none;
    }

    .main-column{
      width: 100%
    }
     .main-blog-area .main-post .main-post-data img{
      float: left;
      width: 50%;
    }

}
  @media only screen and (min-width: 1041px) and (max-width: 1298px){

.col-sm-offset-1{
  margin: 0px;
  padding: 0px;
    margin-top: 9px;
    margin-bottom: 15px;
    width: 81%;
  float: right;
  font-style: italic;
}



}


  </style>

</head>

<body>




<!--the begining of the main header that contains all buttons that direct you to the different pages-->
<nav class="navbar navbar-expand-lg navbar-light m-0 p-0" style="background-color: #000000;border: none;-moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto" style="text-transform: uppercase;  letter-spacing: -1px;font: small-caps bold 13px/30px Georgia, serif;
">
      <li class="nav-item active">
        <a class="nav-link text-light" href="ibyejo">Home</a>
      </li>
       <?php
      global $conn;
      $ViewQuery="SELECT * FROM category ORDER BY id desc";
      $Execute=mysqli_query($conn,$ViewQuery);
      while ($DataRows=mysqli_fetch_array($Execute)) {
        $Id=$DataRows['id'];
        $Category=$DataRows['name'];
        ?>

      <li class="nav-item">
        <a class="nav-link text-light" href="category.php?Category=<?php
        echo $Category; ?>"><?php echo $Category."<br>"; ?></a>
      </li>
     <?php }?>
    </ul>
    <form class="form-inline my-2 my-lg-0" action="search">
      <input class="form-control mr-sm-2" name="Search" type="search" placeholder="Search" aria-label="Search" style="-moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;">
      <button name="SearchButton" class="btn btn-outline-success my-2 my-sm-0" type="submit" style="border: none;background-color: #e40c78;color: #ffffff;-moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;">Search</button>
    </form>
  </div>
</nav>

<!--- Ending of the main header-->

<!------------------------------------------------------------------------------------------------------------------------------->



<div class="container-fluid">
  <div class="col-md-8">

      <div class="row main-row">
        <div class="column main-column" id="main-post">

      <ul class="blog_main-post" style="margin-left: 0;">


         <?php

     global $conn;

     if (isset($_GET["SearchButton"])) {
       $Search=$_GET["Search"];
       $ViewQuery="SELECT * FROM admin_panel WHERE id LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";

     }
//query when category is active
     elseif (isset($_GET["Category"])) {
      $Category=$_GET["Category"];
      $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc ";  
    }
    elseif (isset($_GET["Page"])) {
      $Page=$_GET["Page"];

      if ($Page==0 || $Page<1) {
        $showPostFrom=0;
      }else{

        $showPostFrom=($Page*20)-20;
      }
      

      $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT $showPostFrom,20";
      


    }else{
     $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,3";}

     $Execute=mysqli_query($conn,$ViewQuery);
     while ($DataRows=mysqli_fetch_array($Execute)) {
       $PostId=$DataRows["id"];
       $DataTime=$DataRows["datetime"];
       $title=$DataRows["title"];
       $Category=$DataRows["category"];
       $Admin=$DataRows["author"];
       $Image=$DataRows["image"];
       $Post=$DataRows["post"];
       
       ?>
       <li class="blog_item-post">
                  <a href="detaill?title=<?php echo $title; ?>" style="text-decoration: none;color: black;font-family:'Times New Roman', Times, serif;"> 



                    <div class="col-md-3 blog_item-images">
                      <img class="img-responsive pull-left" width=200;height=800; style="" src="Upload/<?php echo  $Image; ?>">
                    </div>
                    <div class="col-md-9">


                                <h1 class="blog_main-header">
                                  <?php echo htmlentities($title); ?>
                                </h1>

                                 <span id="Published">

             Published on <?php echo htmlentities($DataTime)  ?>

           </span>
           <br>
                      <?php 
           $conn;
           $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
           $ExecuteApproved=mysqli_query($conn,$QueryApproved);
           $RowsApproved=mysqli_fetch_array($ExecuteApproved);
           $TotalApproved=array_shift($RowsApproved);
           if ( $TotalApproved>0) {


            ?>


            <p class=" " style=" font-family:'Times New Roman', Times, serif;">
              <?php echo $TotalApproved; ?> Comments 

            </p>

          <?php } ?>

                 </a>
                     <?php } ?>
                   </div>

        </li>



      </ul>



             <nav>
              <ul class="pagination pull-left pagination-lg">
                <!-- creating the backword button of the pagination--->


                <?php
                if (isset($Page)) {
                

                if($Page>1) {
                 ?>
                 <li><a class="page__link text-dark" href="morePost.php?Page=<?php echo $Page-1; ?>">&laquo;</a></li>

            <?php }   } ?>


            <?php

            global $conn;
            $QueryPagination="SELECT COUNT(*) FROM admin_panel";
            $ExecutePagination=mysqli_query($conn,$QueryPagination);
            $RowPagination=mysqli_fetch_array($ExecutePagination);
            $TotalPost=array_shift($RowPagination);
            //echo $TotalPost;
            $PostPerPage=$TotalPost/20;
            $PostPerPage=ceil($PostPerPage);
           // echo $PostPerPage;

            for ($i=1; $i <=$PostPerPage ; $i++) { 
              if (isset($Page)) {
                            if ($i==$Page) {
                
            ?>
            <li class="active "><a class="page__number text-dark" href="morePost.php?Page=<?php echo $i; ?>" style="background-color: #e40c78;border:none; "><?php echo $i ?></a></li>

          <?php }else{
              ?>
            <li><a class="page__link text-dark" href="morePost.php?Page=<?php echo $i; ?>"><?php echo $i ?></a></li>
            <?php
          }
        }

          } ?>
<!-- creating the forward button of the pagination--->

            <?php
                if (isset($Page)) {
                

                if($Page+1<=$PostPerPage) {
                 ?>
                 <li><a class="page__link text-dark" href="morePost.php?Page=<?php echo $Page+1; ?>">&raquo;</a></li>

            <?php }   } ?>

          </ul>

        </nav>
    </div>

      </div>


  </div>

 
    <div class="column aside-column col-sm-4 col-md-3">
   <div class="row main-row mt-0">
<div class="column aside-column col-md-10 col-sm-12">

  <div class="col-sm-offset-1 col-sm-5 col-md-12 col-sm-12 m-0 p-0 mt-5">
    <h3 class="c-rock-list__title col-sm-9 col-md-12" style="font-size: 19px;letter-spacing: 0px">
      Recent Post
    </h3>
    <div class="c-rock-list__body col-sm-9 col-md-12">
     
     <!------------first recent post------------->

     <span class="c-rock-list__item--body">
      
       <?php
       global $conn;
       $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,1";
       $Execute=mysqli_query($conn,$ViewQuery);
       while ($DataRows=mysqli_fetch_array($Execute)) {
        $Id=$DataRows["id"];
        $Title=$DataRows["title"];
        $DateTime=$DataRows["datetime"];
        $Image=$DataRows["image"];
        if (strlen($DateTime)>16) {
          $DateTime=substr($DateTime, 0,16);
        }
        ?>
        <div class="col-sm-12 col-md-12 p-0 m-0">
                  <a href="detaill?title=<?php echo $Title; ?>" style="text-decoration: none;"> 
          <div class="col-sm-12 col-md-12 p-0 m-0">
           <img class="img-fluid pt-3" style="width: 100%" src="Upload/<?php echo htmlentities($Image) ?>" width=100;height=90;>
         </div>
 <div class="col-sm-12 col-md-12 p-0 m-0">
           <p class="text-dark pt-3" style="font-size: 14px;">
            <?php
            if(strlen($Title)>40){$Title=substr($Title,0,40).'....';}

            echo htmlentities($Title); ?>


          </p>

          <p style="color: #e40c78;"><?php echo htmlentities($DateTime); ?></p>
        </div>

        </a>
      </div>

    <?php } ?> 

  </span>

  <!------------Second recent post------------->

  <span class="c-rock-list__item--body">

  <?php
       global $conn;
       $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 1,1";
       $Execute=mysqli_query($conn,$ViewQuery);
       while ($DataRows=mysqli_fetch_array($Execute)) {
        $Id=$DataRows["id"];
        $Title=$DataRows["title"];
        $DateTime=$DataRows["datetime"];
        $Image=$DataRows["image"];
        if (strlen($DateTime)>16) {
          $DateTime=substr($DateTime, 0,16);
        }
        ?>
        <div class="col-sm-12 col-md-12 p-0 m-0">
                  <a href="detaill?title=<?php echo $Title; ?>" style="text-decoration: none;"> 
          <div class="col-sm-12 col-md-12 p-0 m-0">
           <img class="img-fluid pt-3" style="width: 100%" src="Upload/<?php echo htmlentities($Image) ?>" width=100;height=90;>
         </div>
 <div class="col-sm-12 col-md-12 p-0 m-0">
           <p class="text-dark pt-3">
            <?php
            if(strlen($Title)>40){$Title=substr($Title,0,40).'....';}

            echo htmlentities($Title); ?>


          </p>

          <p style="color: #e40c78;"><?php echo htmlentities($DateTime); ?></p>
        </div>

        </a>
      </div>

    <?php } ?> 

</span>

<!------------third recent post------------->

<span class="c-rock-list__item--body">

  <?php
       global $conn;
       $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 2,1";
       $Execute=mysqli_query($conn,$ViewQuery);
       while ($DataRows=mysqli_fetch_array($Execute)) {
        $Id=$DataRows["id"];
        $Title=$DataRows["title"];
        $DateTime=$DataRows["datetime"];
        $Image=$DataRows["image"];
        if (strlen($DateTime)>16) {
          $DateTime=substr($DateTime, 0,16);
        }
        ?>
        <div class="col-sm-12 col-md-12 p-0 m-0">
                  <a href="detaill?title=<?php echo $Title; ?>" style="text-decoration: none;"> 
          <div class="col-sm-12 col-md-12 p-0 m-0">
           <img class="img-fluid pt-3" style="width: 100%" src="Upload/<?php echo htmlentities($Image) ?>" width=100;height=90;>
         </div>
 <div class="col-sm-12 col-md-12 p-0 m-0">
           <p class="text-dark pt-3">
            <?php
            if(strlen($Title)>40){$Title=substr($Title,0,40).'....';}

            echo htmlentities($Title); ?>


          </p>

          <p style="color: #e40c78;"><?php echo htmlentities($DateTime); ?></p>
        </div>

        </a>
      </div>

    <?php } ?> 
</span>

<!------------link for more post------------->

<div class="c-rock-list__cta">
  <a href="morePost.php?Page=1" class="c-rock-list__more" style="color: #e40c78;text-decoration: none;font-size: 1px;width: 240px;">
    watch more
  </a>
</div>
</div>     

</div>
</div>
</div>
  </div>
</div>






<div class="col-md-12 p-0 m-0 mt-5">
<footer>
  <div id="footer-area" class="m-0 pt-5 p-0">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h3>About WebRoom</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h3>follow us</h3> 
            <ul class="link-area">
              <li><a href=""><i class="fa fa-facebook"></i>albertomelviss@gmail.com</a></li>
              <li><a href=""><i class="fa fa-twitter"></i>Kigali, Rwanda</a></li>    
            </ul>     
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h3>Email Us</h3>
            <ul class="link-area">
              <li><a href=""><i class="fa fa-envelope-o"></i>albertomelviss@gmail.com</a></li>
              <li><a href=""><i class="fa fa-map"></i>Kigali, Rwanda</a></li>    
            </ul>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h3>Our Contact</h3>
            <ul class="link-area">
              <li><a href=""><i class="fa fa-phone"></i>+250786674794</a></li>
              <li><a href=""><i class="fa fa-map"></i>Kigali, Rwanda</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>




</body>
</html>
 
  