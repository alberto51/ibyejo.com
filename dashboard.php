
<?php require_once("include/DB.php"); ?>
<?php require_once("include/Sessions.php"); ?>
<?php require_once("include/functions.php"); ?>

<?php Confirm_Login(); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Dashboard</title>

         <meta charset="UTF-8">
  <meta name="author" content="Niyigena Alberto">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="description" content="tech we news">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript,php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/jquery-3.5.0.min.js"></script>

        <script src="js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="css/adminstyles.css">




    </head>
    <body>

        <div style="height: 10px; background: #27aae1;"></div>
        <nav class="navbar navbar-inverse" role="navigation">

            <div class="container">

                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>


                    </button>
                 <a class="navbar-brand" href="blog.php">   
                    <img style="margin-top: -15px;" src="images/Capture7.PNG" width=80; height=50>
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="collapse">

                <ul class="nav navbar-nav">
                    <li><a href="#">Home</a></li>
                    <li class="active"><a href="blog.php" target="_blank">Blog</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Features</a></li>
                </ul>


                <form action="Dashboard.php" class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search" name="Search">
                    </div>
                    <button class="btn btn-default" name="SearchButton">Go</button>
                </form>
                </div>



            </div>
            
        </nav>

            <div class="Line" style="height: 10px; background: #27aae1;"></div>


        <div class="container-fluid">
            <div class="row">
                
                <div class="col-sm-2">
<br></br>

                    <ul id="Side-Menu" class="nav nav-pills nav-stacked">
                        <li class="active"><a href="Dashboard.php"><span class="glyphicon glyphicon-th"></span>&nbsp;Dashboard</a></li>
                         <li><a href="AddNewPost.php"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Add new Post</a></li>
 <li><a href="categories.php"><span class="glyphicon glyphicon-tags"></span>&nbsp;Categories</a></li>
 <li><a href="admins.php"><span class="glyphicon glyphicon-user"></span>&nbsp;Manage Admin</a></li>
 <li><a href="Comments.php"><span class="glyphicon glyphicon-comment"></span>&nbsp;Comments

 <?php 
                                 $conn;
                                 $QueryTotal="SELECT COUNT(*) FROM comments WHERE status='OFF'";
                                 $ExecuteTotal=mysqli_query($conn,$QueryTotal);
                                 $RowsTotal=mysqli_fetch_array($ExecuteTotal);
                                 $Total=array_shift($RowsTotal);
                                 if ( $Total>0) {
                                    
                                 
                                  ?>


                                 <span class="label pull-right  label-warning">
                                    <?php echo $Total; ?>
                                        
                                    </span>
                                    
                                <?php } ?>


 </a></li>
 <li><a href="blog.php?Page=1" target="_blank"><span class="glyphicon glyphicon-equalizer"></span>&nbsp;Live Blog</a></li>
 <li><a href="Logout.php"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Logout</a></li>
`

                    </ul>
                </div>




                <!-- Ending of side area -->

                <div class="col-sm-10"> <!-- Main area -->

                      <div><?php echo Message(); 
                        echo SuccessMessage();

                        ?></div>
                   <h1>Adminm Dashboard</h1>
                   <div class="table-responsive">
                       <table class="table table-striped table-hover">
                        <tr>
                            <th>NO</th>
                            <th>PostTitle</th>
                            <th>Date & Time</th>
                            <th>Author</th>
                            <th>Category</th>
                            <th>Banner</th>
                            <th>Comments</th>
                            <th>Action</th>
                            <th>Details</th>
                        </tr>

                        <?php
                        global $conn;
                    $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc";
                    $Execute=mysqli_query($conn,$ViewQuery);
                    $SrNo=0;
                    while ($DataRows=mysqli_fetch_array($Execute)) {

                        $Id=$DataRows["id"];
                        $DateTime=$DataRows["datetime"];
                        $Title=$DataRows["title"];
                        $Category=$DataRows["category"];
                        $Admin=$DataRows["author"];
                        $Image=$DataRows["image"];
                        $Post=$DataRows["post"];
                        $SrNo++;
                        
                         ?>


                         <tr>
                             <td><?php echo $SrNo; ?></td>
                             <td style="color: #5e5eff;"><?php
                             if(strlen($Title)>15){$Title=substr($Title,0,15).'....';}


                              echo $Title; ?></td>
                             <td><?php
                             if(strlen($DateTime)>15){$DateTime=substr($DateTime,0,15).'....';}


                              echo $DateTime; ?></td>
                             <td><?php
                              if(strlen($Admin)>15){$Admin=substr($Admin,0,15).'....';}

                              echo $Admin; ?></td>
                             <td><?php
                             if(strlen($Category)>8){$Category=substr($Category,0,8).'....';}

                              echo $Category; ?></td>
                             <td><img src="Upload/<?php echo $Image; ?>" width="160px"; height="60px"></td>
                             <td>
                                 

                                 <?php 
                                 $conn;
                                 $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$Id' AND status='ON'";
                                 $ExecuteApproved=mysqli_query($conn,$QueryApproved);
                                 $RowsApproved=mysqli_fetch_array($ExecuteApproved);
                                 $TotalApproved=array_shift($RowsApproved);
                                 if ( $TotalApproved>0) {
                                    
                                 
                                  ?>


                                 <span class="label pull-right label-success">
                                    <?php echo $TotalApproved; ?>
                                        
                                    </span>

                                <?php } ?>



                                  <?php 
                                 $conn;
                                 $QueryUnApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$Id' AND status='OFF'";
                                 $ExecuteUnApproved=mysqli_query($conn,$QueryUnApproved);
                                 $RowsUnApproved=mysqli_fetch_array($ExecuteUnApproved);
                                 $TotalUnApproved=array_shift($RowsUnApproved);
                                 if ( $TotalUnApproved>0) {
                                    
                                 
                                  ?>


                                 <span class="label  label-danger">
                                    <?php echo $TotalUnApproved; ?>
                                        
                                    </span>
                                    
                                <?php } ?>
                            
                                







                             </td>
                             <td>
                                <a href="EditPost.php?Edit=<?php echo $Id;?>">
                               <span class="btn btn-warning">Edit</span> 
                            </a>

                                <a href="DeletePost.php?Delete=<?php echo $Id;?>"> <span class="btn btn-danger">Delete</span> 
                             </td>
                             <td><a href="FullPost.php?id=<?php echo $Id;?>" target=_blank>
                              <span class="btn btn-primary"> Live Preview</span>
                          </a>
                      </td>
                            
                         </tr>

                         <?php } ?>



                        
                           
                       </table>
                   </div>
                      


                   
                </div>

                <!-- Ending of main area -->
            </div>

            <!-- Ending of row-->
        </div>
            <!-- Ending of container-->

<div id="footer">
    <hr>
    <p>Theme by | Gikundiro koloni | &copy;2019-2020 ---- Allright reserved.</p>
    <a style="color: white; text-decoration: none; cursor: pointer; font-weight: bold;" href="#">
        
<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>

    </a>
   
</div>




    </body>
    </html>

  